# Startup for MPSoS:SC-IOC-900
#
# Module: essioc
#
require essioc


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")


#
# IOC: MPSoS:SC-IOC-900
# Load iocsh file
#
iocshLoad("$(E3_CMD_TOP)/iocsh/mpsos.iocsh")
